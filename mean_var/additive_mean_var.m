function [mean, var] = additive_mean_var(model, x,i)

% INPUT
% model : current gp model
% x : evaluation point (each point is 1XD row vector)
% i: index of the additive kernel
%
% OUTPUT 
% mean and variance of f^(i)(x) given data (except noise)
%
% VAR
% k_xx : kernel matrix of the point
% k_Nx : kernel matrix between data and the point 
%
if ~strcmp(model.kernel_type,'additive'),error('the model is not set to additive'); end

k_xx = model.cov_model(model.hyp, x, x,i);
k_Nx = model.cov_model(model.hyp, model.X, x,i);

intermediate=model.L'\(model.L\k_Nx);

mean = model.prior_mean+intermediate'*(model.f-model.prior_mean);
var = k_xx - k_Nx'*intermediate;
end