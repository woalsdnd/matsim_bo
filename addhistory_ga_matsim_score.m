function [state,options,optchanged ] = addhistory_ga_matsim_score(options,state,flag)
    optchanged = false;
    
    %set name of the output file
    out_dir_name=[getenv('HOME_DIR') 'results/ga/'];
    if ~exist(out_dir_name, 'dir'), mkdir(out_dir_name);    end
    out_file_name=[out_dir_name 'ga_result_score.mat'];
    
    scores=state.Score;
    
    %save results of the experiment
    if exist(out_file_name, 'file') == 2
        a=load(out_file_name);
        scores=[a.scores;scores];
        save(out_file_name,'scores','-append');
    else
        save(out_file_name,'scores');
    end
end
