function plotEachIteration(model,next_x,plotIteration)
%   plot the GP model for each iteration
%
%   INPUT : model, next point, plotIteration   
%

if strcmp(plotIteration,'plot')
    % plot the objective function once
    if model.n==model.n_init_pts
        plotFun(model);
    end
    
    plotGPmodel(model,next_x,1,1,1);
%     plotGPmodel_dcop(model,0,next_x,0)
%     plotLogLikelihood(model);

end



end

