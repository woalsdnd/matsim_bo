function ga_matsim_tt
    %set function handler and number of variables    
%     fun=@schwef;
%     nvars=100;
%     fun=@MATSIM_score;
    fun=@MATSIM_travel_time;
    nvars=77;

    %set options
    lb=-ones(1,nvars);
    ub=ones(1,nvars);
    options=gaoptimset('Generations',18,'OutputFcn',@addhistory_ga_matsim_tt);
    
    %run genetic algorithm
    [x,fval,exitFlag,output,population,scores] = ga(fun,nvars,[],[],[],[],lb,ub,[],options);

end
