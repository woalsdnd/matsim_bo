function run_experiment(fun_name,n_runs,n_iterations,n_init_pts,high_dim,projection_dim,hyp_update_cycle,exp_num,plotIteration,method)
%% set output directory ,experiment setting function handler, objective function
fh=str2func(['exp_set_' fun_name]);
if strcmp(fun_name,'matsim_score')
    objective_function=@MATSIM_score;
    high_dim=77;
elseif strcmp(fun_name,'matsim_travel_time')
    objective_function=@MATSIM_travel_time;
    high_dim=77;
elseif strcmp(fun_name,'MATSIM_score_expressways')
    objective_function=@MATSIM_score_expressways;
    high_dim=27;
elseif strcmp(fun_name,'MATSIM_travel_time_expressways')
    objective_function=@MATSIM_travel_time_expressways;
    high_dim=27;
else
    objective_function=analytic_fun(fun_name,high_dim);
end        

%% iterate methods
% 1:original BO
% 2:random basis 
% 3:random projection
% 4:subspace_learning
% 5:additive model
for run=1:n_runs
    %for method=1:5
        % initialize a model with the experiment settings 
        model=fh(method,objective_function,n_init_pts,n_iterations,high_dim,projection_dim,hyp_update_cycle);
        
        % run Bayesian Optimization
        [X,Y,opt_pts,opt_values,model]=BayesianOptimization(model,plotIteration);

        % save the results
        save_results(fun_name,Y,opt_values,run,method,exp_num);

        % save function values
        save_f_values(fun_name,model.f,run,method,exp_num);
    %end
end

end
