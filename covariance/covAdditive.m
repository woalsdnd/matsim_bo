function K = covAdditive(hyp, x, z,i)
% input : hyp = [ log(l_1)
%                 log(l_2)
%                    .
%                 log(l_D)
%                 log(sqrt(sf2)) ]
%
%         x (data points, each row is a data point)
%         z (data points, each row is a data point)
%         i (index)
%         (i==0 -> full covariance, 
%          i==1 -> k^1) 
%
% return : k^i, a kernel of (i)th variable
%
% (simple) Additive covariance fun based on Squared Exponential covariance function.
%
% k^i(zi,x'_i) = sf2 * exp(-(zi - x'_i)^2/(2*l_i^2))
% 
% CAVEAT
% There would be other different ways of grouping variables 
% but only a simple case (each group representing each variable) is
% implemented here
 
%check dimensions
[n,D] = size(x); [n_,D_]=size(z); 
if ~D==D_ ,return; end

% retrieve hyper parameters (length scales, signal)
l = exp(hyp(1:D));                               
sf2 = exp(2*hyp(D+1));                           

% calculate necessary kernels in additive model
if i==0 %K_DD or K_Dx (kernel matrix for full kernel)
    % K_add(k,m,:)=exp(-(x(k,:)-z(m,:)).^2./(2*l'.^2))
    xz = -bsxfun(@minus,permute(x,[1 3 2]),permute(z,[3 1 2])).^2; 
    K_add = exp(bsxfun(@rdivide,xz,permute(2*l'.^2,[1 3 2])));    
    
    % take summation of all kernels
    K=sum(K_add,3); 
else
    % K_add(k,m,i)=exp(-(x(k,i)-z(m,i))^2/(2*l(i)^2))
    xz = -bsxfun(@minus,x(:,i),permute(z(:,i),[2 1])).^2; 
    K = exp(xz/(2*l(i)^2));
end

% multiply with signal
K=sf2*K;

end
