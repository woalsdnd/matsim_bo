function [init_pts,init_fs]=random_selection(obj_fun,n_init_pts,dimension,bound_inits,bounds_domain)
%   INPUT : the number of initial points
%           input dimension        
%           bound of initial points (1X2)
%           bounds of input domain (dimensionX2)
%
%   OUTPUT : initial points
%            initial function values
%

%draw uniformly random samples within the bounds_domain
init_pts=unifrnd(repmat(bounds_domain(:,1)',n_init_pts,1),repmat(bounds_domain(:,2)',n_init_pts,1),n_init_pts,dimension);
init_fs=zeros(size(init_pts,1),1);
for i=1:size(init_pts,1)
    i
    f_value= obj_fun(init_pts(i,:));
    while ~isempty(bound_inits) && (f_value>bound_inits(2) || f_value<bound_inits(1)) 
        init_pts(i,:)=unifrnd(bounds_domain(:,1)',bounds_domain(:,2)',1,dimension);
        f_value= obj_fun(init_pts(i,:));
    end
    init_fs(i,1)=f_value;
end 
    
    
    
end