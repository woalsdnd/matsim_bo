function optimum=get_optimum(fun_name)
% return the optimum value of the MAXIMIZATION problem
%
%   INPUT : function name
%
%   OUTPUT : optimal value
%

if strcmp(fun_name,'branin')
    optimum=-0.397887;
else
    optimum=0;
end

end
