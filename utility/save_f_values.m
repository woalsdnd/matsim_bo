function save_f_values(fun_name,Y,run,method,exp_num)
%save simple regret & average cumulative regret
%

%set name of the output file
out_dir_name=[getenv('HOME_DIR') 'results/' fun_name '/'];
if ~exist(out_dir_name, 'dir'), mkdir(out_dir_name);    end
out_file_name=[out_dir_name 'f_values' num2str(exp_num) '.mat'];

%load previously saved data
if exist(out_file_name, 'file') == 2,   load(out_file_name);    end

%concat the result
f{run,method}=Y;

%save results of the experiment
if exist(out_file_name, 'file') == 2
    save(out_file_name,'f','-append');
else
    save(out_file_name,'f');
end


end

