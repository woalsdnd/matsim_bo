function [A, init_pts, init_fs] =learn_subspace(n_centers,projection_dim,high_dim,obj_fct)
	%set step size
	h=0.03;
   
    %randomly pick points from [-1,1]^D
	x=unifrnd(-1+h,1,n_centers,high_dim);

	%evaluate function values
	pts=zeros((high_dim+1)*n_centers,high_dim);
	fs=zeros((high_dim+1)*n_centers,1);
	for i=1:n_centers
	    pts((i-1)*(high_dim+1)+1,:)=x(i,:);
	    fs((i-1)*(high_dim+1)+1,1)=obj_fct(x(i,:));
	    for j=2:high_dim+1
		dir=zeros(1,high_dim);
		dir(1,j-1)=h;
		pts((i-1)*(high_dim+1)+j,:)=x(i,:)-dir;
		fs((i-1)*(high_dim+1)+j,1)=obj_fct(x(i,:)-dir);
	    end
	end

	%calculate derivative
	D=zeros(n_centers,high_dim);
	for i=1:n_centers
	    for j=1:high_dim
		D(i,j)=(fs((i-1)*(high_dim+1)+j+1,1)-fs((i-1)*(high_dim+1)+1,1))/h;
	    end
	end

	%run SVD and return results
	[U,S,V]=svd(D');
	A=U(:,1:projection_dim);
	init_pts=pts*A;
	init_fs=fs;

end

