function next_point=retrieve_next(model)
%% retrieve the next point differently depending on the methods (represented by kernels)
    if strcmp(model.kernel_type,'additive')
        next_point=retrieve_additive(model);
    elseif strcmp(model.kernel_type,'ard')
        dopt.maxevals=20*model.high_dim;    
        dopt.maxits = 10*model.high_dim;
        problem.f = @(x) -SEard_acq(model, x');
        [fmin,xmin, history] = direct(problem, model.bounds,dopt);
        opt = optimset('GradObj','off','Display','off','MaxIter', 100,'Algorithm', 'sqp','TolCon', 1e-15, 'TolFun', 1e-15);
        optimizer=fmincon(problem.f,xmin,[],[],[],[],model.bounds(:,1),model.bounds(:,2),[],opt);
        next_point=optimizer';        
    end
end