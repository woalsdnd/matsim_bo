%% set initial environment (codes modified from startup.m in gpml)

% clear all windows, command lines
clc;clear;close all;

disp('setting up initial environment...');

% get the filename and current directory (which is used as home)
this = mfilename;
home_dir = which(this); home_dir = home_dir(1:end-2-numel(this));
if exist('OCTAVE_VERSION') ~= 0 && numel(home_dir)==2 
  if strcmp(home_dir,'./'), home_dir = [pwd,home_dir(2:end)]; end
end 

% add paths
addpath(genpath(home_dir(1:end-1)));

%set library path (for calling python) & home directory
setenv('LD_LIBRARY_PATH', '');
setenv('HOME_DIR',home_dir);
clear this home_dir; 