%run_experiment(fun_name,n_runs,n_iterations,n_init_pts,high_dim,projection_dim,hyp_update_cycle,exp_num,plotIteration)
% 
% plotIteration : 'plot' or any other strings (no plot)
%
% REQUIREMENT
% mod (n_init_pts,hyp_update_cycle)==0
% (to update Maximum Spanning Tree everytime hypers are updated)
% mod (n_iterations,hyp_update_cycle)==hyp_update_cycle-1 
% (to avoid model update at the last iteration)
%

startup;

run_experiment('matsim_travel_time',1,299,100,77,5,50,11,'noplot',1);
run_experiment('matsim_travel_time',1,299,100,77,5,50,13,'noplot',3);
run_experiment('matsim_travel_time',1,299,100,77,5,50,14,'noplot',4);
run_experiment('matsim_travel_time',1,299,100,77,5,50,15,'noplot',5);
