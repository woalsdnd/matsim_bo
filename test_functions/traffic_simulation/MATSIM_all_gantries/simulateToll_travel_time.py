import subprocess
import time
import os
import shutil
import sys
from IPython.utils.io import stdout, stderr
import xml.etree.cElementTree as ET

def simulateToll(var_file, serial_num):
    
    project_folder = "/home/jaemin/workspace_java/MATSim_RoadPricing"
    
    # load toll links
    lines = [line.rstrip('\n') for line in open(project_folder + "/config/toll_links")]
    
    # load variables
    x = [line.rstrip('\n') for line in open(var_file)]
    
    # create xml structure
    root = ET.Element("roadpricing", type="link", name="singapore erp")
    links = ET.SubElement(root, "links")
    
    # specify the price according to var_file
    i = 0
    for link in lines:
        link_tuple = ET.SubElement(links, "link", id=str(link))
        ET.SubElement(link_tuple, "cost", start_time="07:30", end_time="09:00", amount=str(x[i]))  # only care about morning rush hours 
        i = i + 1
    
    # output into xml    
    xml = ET.ElementTree(root)
    with open(project_folder + "/config/TOLL/toll_%s.xml" % (serial_num), 'w') as f:
        f.write('<?xml version="1.0" encoding="UTF-8" ?><!DOCTYPE roadpricing SYSTEM "/home/jaemin/workspace_java/MATSim_RoadPricing/config/roadpricing_v1.dtd">')
        xml.write(f, 'utf-8')
    
    # create config_serial_num.xml chagning output folder name
    config_folder = "/home/jaemin/workspace_java/MATSim_RoadPricing/config"
    with open(os.devnull, "w") as f:
        subprocess.call("sed \"s/matsim_output/matsim_output_%s/g;s/toll.xml/toll_%s.xml/g\" <%s/config_template.xml >%s/CONFIG/config_%s.xml" % (serial_num, serial_num, config_folder, config_folder, serial_num), shell=True, stdout=f, stderr=f)
    
    # run MATSIM
    cp = "%s:%s/libs/*" % (project_folder, project_folder)
    config = "%s/config/CONFIG/config_%s.xml" % (project_folder, serial_num)
    subprocess.call("java -Xmx1g -cp %s org.matsim.run.Controler %s" % (cp, config),
    shell=True, stdout=open(config_folder + "/LOG/log_%s" % (serial_num) , "w"), stderr=open(config_folder + "/LOG/err_log_%s" % (serial_num), "w"))
    
    # read scores from the file (max of last two scores to cover the case of decrement)
    lines = [line.rstrip('\n') for line in open(project_folder + "/MATSIM/matsim_output_%s/ITERS/it.15/15.tripdurations.txt" % (serial_num)) ]
    travel_time = float(lines[len(lines) - 1].split(" ")[3])

    print -travel_time
    
    shutil.rmtree(project_folder + "/MATSIM/matsim_output_%s" % (serial_num))
    
#     lines = [line.rstrip('\n') for line in open(project_folder + "/MATSIM/matsim_output_%s/traveldistancestats.txt" % (serial_num))]
#     distance = float(lines[len(lines) - 1].split("\t")[1])
    
#     hourly_penalty = 79.02;
#     dist_penalty = 0.000294;
#     result = (-score - distance * dist_penalty) / (2 * hourly_penalty);
#     print result
    
    # move the result folder    
    # os.rename(project_folder + "/MATSIM/matsim_output", project_folder + "/MATSIM/matsim_output_%f" % (time.clock()))
        
    
if __name__ == '__main__':
    simulateToll(sys.argv[1], sys.argv[2])
