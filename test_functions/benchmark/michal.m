function y=michal(x,Q)
%rotate the basis
    x=x*Q;

d = length(x);
m = 10;
sum = 0;

%rescale the domain to [0,pi]^*
bounds = [-0,pi];
x(1:d) = bounds(1) + ...
    ((x(1:d)+1)/2)*(bounds(2) - bounds(1));

for ii = 1:d
	xi = x(ii);
	new = sin(xi) * (sin(ii*xi^2/pi))^(2*m);
	sum  = sum + new;
end

y = -sum;

end



