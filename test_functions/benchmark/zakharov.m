function y=zakharov(x)

d = length(x);

%rescale the domain to [-5,10]^*
bounds = [-5,10];
x(1:d) = bounds(1) + ...
    ((x(1:d)+1)/2)*(bounds(2) - bounds(1));

sum1 = 0;
sum2 = 0;

for ii = 1:d
	xi = x(ii);
	sum1 = sum1 + xi^2;
	sum2 = sum2 + 0.5*ii*xi;
end

y = sum1 + sum2^2 + sum2^4;

end



