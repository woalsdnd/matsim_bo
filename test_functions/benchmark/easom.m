function y=easom(x,Q)

%rotate the basis
x=x*Q;

%rescale the domain to [-100,100]X[-100,100]
bounds = [-100,100; -100,100];
x(1:2) = bounds(1:2, 1)' + ...
    (x(1:2)+1)/2.*(bounds(1:2, 2) - bounds(1:2, 1))';


x1 = x(1);
x2 = x(2);

fact1 = -cos(x1)*cos(x2);
fact2 = exp(-(x1-pi)^2-(x2-pi)^2);

y = fact1*fact2;

end



