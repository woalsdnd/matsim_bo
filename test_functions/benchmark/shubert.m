function y=shubert(x,Q)

%rotate the basis
x=x*Q;

%rescale the domain to [-10,10]X[-10,10]
bounds = [-10,10; -10,10];
x(1:2) = bounds(1:2, 1)' + ...
    (x(1:2)+1)/2.*(bounds(1:2, 2) - bounds(1:2, 1))';

x1 = x(1);
x2 = x(2);
sum1 = 0;
sum2 = 0;

for ii = 1:5
	new1 = ii * cos((ii+1)*x1+ii);
	new2 = ii * cos((ii+1)*x2+ii);
	sum1 = sum1 + new1;
	sum2 = sum2 + new2;
end

y = sum1 * sum2;

end



