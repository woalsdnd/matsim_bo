function acq = additive_acq(model, xi,i)
%% return acquisition function (GP-UCB) for k^i
% each group is each dimension
%

if (i<=0),return,end
x=zeros(1,model.high_dim);  x(1,i)=xi;
[mu, var] = additive_mean_var(model, x,i);

sigma = sqrt(var);

% GP-UCB
% coeff is following the paper of additive models for experiment (chapter 4.4)
t=model.n;
coeff = 0.2*log(2*t);
acq = mu+sqrt(coeff)*sigma;
end
